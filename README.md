# CoLab Developer Candidate Code Sample
## Deliverables 
* Git Repository for **Ruby Functions** 
* Git Repository for the **Sample Rails App**

### Ruby Functions
Inside the `catdog_and_functions` directory you will have a `test.rb` file and a `my_functions.rb` file.
Follow the instructions inside of `my_functions.rb`. You will know you are successfull if all the tests pass by running the test file  `ruby test.rb`

### Sample Rails App
Generate a rails app to show airplane data using the following guidelines: 

* Use the provided `airplanes.csv` to populate your DB however you see fit. (hint: "seed")
* You do not need to style any pages
* From the web interface, the user should be able to: 
  1. create a new airplane
  2. see all existing airplanes
  3. delete an airplane
* You do not need to worry about Authentication or Permissions. 