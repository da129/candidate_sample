# Write a function which multiplies 2 numbers without using the * operator
# WRITE YOUR CODE HERE.  Name your function `multiply`.


# ================================================================================= #
# Write a function which returns:
#
#   * "Cat" if the number is divisible by 3
#   * "Dog" if the number is divisible by 5
#   * "CatDog" if the number is divisible by 3 and 5
#   * Otherwise, return the number itself
#
# Remember that the % operator (modulo) is your friend.  It returns a zero if one
# number is divisible by another number.  In other words, 4 % 2 == 0.

# WRITE YOUR CODE HERE.  Name your function `catdog`.